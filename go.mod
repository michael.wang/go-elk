module github.com/pcherednichenko/go-elastic-example

go 1.15

require (
	github.com/pkg/errors v0.8.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
)
